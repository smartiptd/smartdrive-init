#!/bin/sh

########################### PARAMETERS ###############################
MONGOS_PORT=27017
CONFIGDB=cfg/smartdrive-1:27019,smartdrive-2:27019,smartdrive-3:27019
MONGOS_LOG=/media/data/smartdrive/mongodb/log/mongos.log
######################################################################

# Kill previous process
kill -9 $(lsof -t -i:$MONGOS_PORT) 2> /dev/null

# Start Mongod process
mongos --port $MONGOS_PORT --configdb $CONFIGDB --fork --logpath $MONGOS_LOG
