#!/bin/sh

################ PARAMETERS ####################
SHARDSVR_PORT=27018
RS_NAME=rs0
DB_PATH=/media/data/smartdrive/mongodb/db/
DB_LOG=/media/data/smartdrive/mongodb/log/db.log
################################################

# Kill previous process
kill -9 $(lsof -t -i:$SHARDSVR_PORT) 2> /dev/null

# Start Mongod process
mongod --shardsvr --port $SHARDSVR_PORT --dbpath $DB_PATH --replSet $RS_NAME --fork --logpath $DB_LOG --rest
