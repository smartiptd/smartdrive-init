#!/bin/sh

##################### PARAMETERS #########################
CONFSVR_PORT=27019
RS_NAME=cfg
CONFSVR_PATH=/media/data/smartdrive/mongodb/confsvr/
CONFSVR_LOG=/media/data/smartdrive/mongodb/log/confsvr.log
##########################################################

# Kill previous process
kill -9 $(lsof -t -i:$CONFSVR_PORT) 2> /dev/null

# Start Mongod process
mongod --configsvr --port $CONFSVR_PORT --dbpath $CONFSVR_PATH --replSet RS_NAME --fork --logpath $CONFSVR_LOG
