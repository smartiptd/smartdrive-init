#!/bin/sh

# *** SETUP PARAMETERS *** #
RS_NAME=smartdrive_rs0
# ************************ #

# Shell script for start local mongodb cluster

# Constant
MONGO_PORT=27017
MONGOS_PORT=27018
CONFSVR_PORT=27019
DB_PORT=27020

DATA_PATH=/data
DB_PATH=$DATA_PATH/db
LOG_PATH=$DATA_PATH/log
CONFSVR_PATH=$DATA_PATH/confsvr

DB_LOG=$LOG_PATH/db.log
CONFSVR_LOG=$LOG_PATH/configsvr.log
CONFDB_LOG=$LOG_PATH/configdb.log

DIR=$(readlink -f .)
CMD_RS="$DIR/command.rs.js"
CMD_SHARD="$DIR/command.shard.js"

HOST=$(hostname)

###############################
# Create dependency directory #
###############################
echo "Create dependency directories "

mkdir -p $DATA_PATH
echo "  * $DATA_PATH"

mkdir -p $DB_PATH
echo "  * $DB_PATH"

mkdir -p $LOG_PATH
echo "  * $LOG_PATH"

mkdir -p $CONFSVR_PATH
echo "  * $CONFSVR_PATH"

echo DONE

#############################
# Prepare Mongo Environment #
#############################
echo "Prepare Mongo Environment"
# Kill previous pid
echo "  * Kill previous pid"
kill -9 $(lsof -t -i:$MONGO_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$MONGOS_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$DB_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$CONFSVR_PORT) 2> /dev/null

# Show leftover mongo service
echo "  * Show leftover mongo service"
ps -ax | grep mongo

# Restart mongo service
echo "  * Restart mongo service"
service mongod --full-restart
echo DONE

#######################
# Start Mongo Service #
#######################
echo "Start Mongo Service"

########## 1) Replication ##########
echo "1) Replication"
# Start each member of the replica set with the appropriate options
echo "  * Start each member of the replica set with the appropriate options"
mongod --shardsvr --dbpath $DB_PATH --port $DB_PORT --replSet $RS_NAME --rest --fork --logpath $DB_LOG

sleep 5

# Initiate the replica set
echo "  * Initiate the replica set"
mongo $HOST:$DB_PORT/admin $CMD_RS
echo DONE

sleep 5

########## 2) Clustering ##########
echo "2) Clustering"
# Start the Config Server Database Instances
echo "  * Start the Config Server Database Instances"
mongod --configsvr --dbpath $CONFSVR_PATH --port $CONFSVR_PORT --fork --logpath $CONFSVR_LOG

sleep 5

# Start the mongos Instances
echo "  * Start the mongos Instances"
mongos --configdb $HOST:$CONFSVR_PORT --fork --logpath $CONFDB_LOG

sleep 5

# Add Shards to the Cluster
echo "  * Run command for add shard"
mongo $HOST:$MONGO_PORT/admin --eval "var param1=\"$RS_NAME\", param2=\"$HOST\", param3=\"$DB_PORT\"" $CMD_SHARD
echo DONE
