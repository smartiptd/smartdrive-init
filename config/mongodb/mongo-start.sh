#!/bin/sh

# Constant
MONGO_PORT=27017
MONGOS_PORT=27018
CONFSVR_PORT=27019
DB_PORT=27020

#############################
# Prepare Mongo Environment #
#############################
echo "Prepare Mongo Environment"
# Kill previous pid
echo "  * Kill previous pid"
kill -9 $(lsof -t -i:$MONGO_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$MONGOS_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$DB_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$CONFSVR_PORT) 2> /dev/null

# Show leftover mongo service
echo "  * Show leftover mongo service"
ps -ax | grep mongo

# Restart mongo service
echo "  * Restart mongo service"
service mongod --full-restart
echo DONE

#######################
# Start Mongo Service #
#######################
echo "Start Mongo Service"

echo "  * Start the member of the replicat set"
mongod -f /etc/mongod_shardsvr.conf

echo "  * Start the config server database instances"
mongod -f /etc/mongod_confsvr.conf

echo "  * Start the mongos instances"
mongos -f /etc/mongos_confdb.conf
