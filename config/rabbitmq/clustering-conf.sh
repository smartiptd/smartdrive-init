#!/bin/sh

# *** SETUP PARAMETERS *** #
MASTER_HOSTNAME=smartdrive-1
# ************************ #

echo "#######################"
echo "# RabbitMQ Clustering #"
echo "#######################"

echo " Noted: "
echo " - Copy the Erlang Cookie (/var/lib/rabbitmq/.erlang.cookie)"
echo " - Stop the server before updating the cookie (service rabbitmq-server stop/start)"
echo " - Kill rabbitmq process if cannot start service (kill -9 <port = ps aux | grep erl>)"
echo ""

echo "  * Setup the master"
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl start_app

#echo " * Setup the slaves"
#rabbitmqctl stop_app
#rabbitmqctl reset
#rabbitmqctl join_cluster rabbit@$MASTER_HOSTNAME
#rabbitmqctl start_app

echo "  * Display clustering status"
rabbitmqctl cluster_status

echo "  * Add new RabbitMQ user"
rabbitmqctl add_user smartdrive IPTD@dmin
rabbitmqctl set_user_tags smartdrive administrator

echo "  * Set user permission"
rabbitmqctl set_permissions -p / smartdrive ".*" ".*" ".*"
