#!/bin/sh

echo "###########"
echo "# OpenSSL #"
echo "###########"

echo "  * Install openssl application"
apt-get install openssl

echo "  * Display version"
openssl version 
