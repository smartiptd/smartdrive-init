#!/bin/sh

echo "############"
echo "# RabbitMQ #"
echo "############"

echo "  * Create a list file for RabbitMQ"
echo "deb http://www.rabbitmq.com/debian/ testing main" >> /etc/apt/sources.list

echo "  * Install RabbitMQ"
apt-get install -y aptitude
aptitude update
aptitude install rabbitmq-server -y

echo "  * Enable web management"
rabbitmq-plugins enable rabbitmq_management

echo "  * Restart RabbitMQ service"
service rabbitmq-server restart
