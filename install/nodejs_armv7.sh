#!/bin/sh

echo "###########"
echo "# Node.js #"
echo "###########"

cd /tmp

echo "  * Install dependency packages"
apt-get install make g++ python -y

echo "  * Download Node.js package"
wget https://nodejs.org/dist/v4.2.4/node-v4.2.4-linux-armv7l.tar.gz

echo "  * Unzip package"
tar zxf node-v4.2.4-linux-armv7l.tar.gz

# Prepare directory
mkdir -p /prog

echo "  * Copy program to specified path"
cp node-v4.2.4-linux-armv7l/ /prog/

echo "  * Set environment PATH follow this step"
echo "      1) Modify environment file (vi /etc/environment)"
echo "         with node.js path > PATH=\"...:/prog/node-v4.2.4-linux-armv7l/bin\""
echo "      2) Execute new configuration (source /etc/environment)"
echo "      3) Add execute command to system-wide shell scrip"
echo "         (vi ~/.bashrc >>"
echo "          # Execute environment path"
echo "          source /etc/environment)"
echo "      4) Execute new shell configuration (source ~/.bachrc)"
