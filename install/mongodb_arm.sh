#!/bin/sh

echo "###################"
echo "# MongoDB for ARM #"
echo "###################"

echo "  * Installation Step"

echo "    1) Download binaries"
echo "#Clone this repository"
echo "git clone https://github.com/Barryrowe/mongo-arm.git"

echo "    2) Create the mongo user"
echo "sudo useradd mongo"
echo "sudo passwd mongo"

echo "    3) Copy the mongo binaries to the desired install location"
echo "#example:"
echo "cp -R binaries/2.1.1 /prog/mongo"

echo "    4) Modify environment file"
echo "vi /etc/environment"
echo "PATH=\"...:/prog/mongo/2.1.1/bin\""

echo "    5) Execute new configuration"
echo "source /etc/environment"

echo "    6) Add execute command to system-wide shell scrip"
echo "vi ~/.bashrc"
echo "# Execute environment path"
echo "source /etc/environment"

echo "    7) Execute new shell configuration"
echo "source ~/.bachrc"

echo "    8) Change the ownership of the installed mongo binaries so they are owned by the mongo user"
echo "chown -R mongo:mongo /path/to/installed/binaries"

echo "    9) Creat the /data/db directory for MongoDB's data storage"
echo "sudo mkdir /data"
echo "sudo mkdir /data/db"

echo "    10) Change the ownership of the /data/db directory so it is owned by the mongo user"
echo "sudo chown mongo:mongo /data/db"

echo " "
echo "  * Set As Service Step (Debian)"

echo "    1) Copy config/mongodb to the init.d location"
echo "sudo cp config/mongodb /etc/init.d/mongodb"

echo "    2) Update /etc/init.d/mongodb to point to your install path from step 3"
echo "A. Line 50 will need to be modified to point to the mongos executable from your install path in step 3. (:/prog/mongo/2.1.1/bin)"
echo "B. If you created a user with name other than \"mongo\" in step 2 you will need to update Line 95 with the user you configured."

echo "    3) Update the permissions for the mongodb init.d file so it can be executed"
echo "sudo chmod 755 /etc/init.d/mongodb"

echo "    4) Copy the config/mongod.conf file to /etc/mongod.conf"
echo "sudo cp config/mongod.conf /etc/mongod.conf"

echo "    5) Register the mongodb service"
echo "sudo update-rc.d mongodb defaults"

echo "    6) Startup the service"
echo "sudo service mongodb start"
