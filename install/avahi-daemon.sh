#!/bin/sh

PATH_CONFIG_AVAHI=../config/avahi
PATH_DEFAULT_CONFIG_AVAHI=/etc/avahi/services

FILE_CONFIG_AVAHI=$PATH_CONFIG_AVAHI/afpd.service
FILE_CONFIG_AVAHI_MODE=/etc/default/avahi-daemon

echo "################"
echo "# Avahi-Daemon #"
echo "################"

echo "  * Install Avahi-Daemon"
apt-get install Avahi-Daemon

echo "  * Update setup"
update-rc.d avahi-daemon defaults

echo "  * Create service configuration file"
cp $FILE_CONFIG_AVAHI $PATH_DEFAULT_CONFIG_AVAHI

echo "  * Change avahi detect mode"
sed -i 's/AVAHI_DAEMON_DETECT_LOCAL=1/AVAHI_DAEMON_DETECT_LOCAL=0/g' $FILE_CONFIG_AVAHI_MODE

echo "  * Start service"
service avahi-daemon start
