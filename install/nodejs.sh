#!/bin/sh

echo "####################################"
echo "# Node.js(v0.10.31) & NPM(v1.4.23) #"
echo "####################################"

cd /tmp

echo "  * Install dependency packages"
apt-get install make g++ python -y

echo "  * Download Node.js package"
wget http://nodejs.org/dist/v0.10.31/node-v0.10.31.tar.gz

echo "  * Unzip package"
tar zxf node-v0.10.31.tar.gz
cd  node-v0.10.31

echo "  * Compile & Install"
make all
make install
